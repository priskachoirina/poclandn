function [poc, outputAll] = getPOC( imgBlockCur, imgBlockRef ,frame, type)
%GETPOC Summary of this function goes here
%   Detailed explanation goes here
    
    mb_x = 9;
    mb_y = 9;   
    
    T       = mb_x;
    range   = 1:T;
    window  = hann(T);
    window  = window.'*window;
    
    im0     = double(imgBlockCur);
    im1     = double(imgBlockRef);
    
%     figure(1); imagesc(im0);axis equal; axis tight; hold on 
    
    [cols, rows, ~] = size(im0);
    
    cy   = floor(cols/mb_y);
    cx   = floor(rows/mb_x);

    outputAll  = cell(cy*cx,13);
       
    poc  = zeros(mb_x,mb_y);
       
    num  = 1;        
    
    for y = 1 : (cols /mb_y)
        for x = 1 : (rows/mb_x)
            corX = x*mb_x;
            corY = y*mb_y;
            
            block_ref   = imcrop(im0,[corX,corY,mb_x,mb_y]);
            block_curr  = imcrop(im1,[corX,corY,mb_x,mb_y]);
            
%             t = text(x*mb_x,y*mb_y, [num2str(x) ',' num2str(y) ' | ' num2str(num)]);
%             t.FontSize = 9;
%             t.Color = 'r';
            
            fft_ref  = fft2(block_ref.*window,mb_y,mb_x);
            fft_curr = fft2(block_curr.*window,mb_y,mb_x);
            
            R1  = fft_curr.*conj(fft_ref);
            R2  = abs(R1);
            R2(R2==0)=1e-31;
            R   = R1./R2;
            r   = fftshift(abs(ifft2(R)));
            
            poc(:,:,num) = r;
            
            [temp_y, temp_x]=find(r==max(max(r)));
            
            if(size(temp_y,1) > 1 || size(temp_x,1) > 1)
                temp_y = 5;
                temp_x = 5;
            end
 
            data(num,:) = {frame,num,temp_x,temp_y,x,y,max(max(r)),type,cx,cy,imgBlockCur,corX,corY};
            num = 1+num;            
        end
    end
    
    outputAll = cell2table(data,'VariableNames',{'Frame';'Blok_Ke'; 'RealX';'RealY';'BLokX';'BlokY'; 'NilaiMaxPOC';'Type';'Qx';'Qy';'ImgBlock';'corX';'corY'});         
    
end