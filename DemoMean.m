% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
% % Left Eyebrow    = 1 - 5
% % Right Eyebrow   = 6 - 10
% % Left Eyes       = 20 - 25
% % Right Eyes      = 26 - 31
% % Nose            = 11 - 19
% % mouth           = 32 - 49
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %

clear; close all; clc;
addpath(genpath('.'));

% % Load Models
fitting_model='models/Chehra_f1.0.mat';
load(fitting_model);    

FD      = vision.CascadeObjectDetector();

% % Test Path
nmFold    = 'disgust/03_EP09_03';
nFold   = ['test_images/sequence/' nmFold '/'];
imFol   = dir(fullfile(nFold,'*.jpg'));
imFol   = natsortfiles({imFol.name});

% See getFeatures.m
% Tipe Feature : AlisKiri, AlisKanan, MataKanan, MataKiri, Mulut,All 
tipeFeat   = 'Mulut';
nmFoldRes  = ['results/sequence/' nmFold];
sMax       = size(imFol,2);

% mkdir(nmFoldRes);
disp('Get Data.........');

if(sMax > 0)
    for frame = 1:numel(imFol)-1
        % % name file
        nameFile1 = [nFold imFol{frame}];
        nameFile2 = [nFold imFol{frame}];
      
        % % Load Image
        img0 = imread(nameFile1);
        img1 = imread(nameFile2);
%         imshow(img0)
%         figure(1), imshow(img0); title(['Frame : ' num2str(frame) ' Filename : ' nameFile1]);
%         figure(2), imshow(img1); title(['Frame : ' num2str(frame+1)]);
        
        if frame == 1
             bbox = step(FD, img0);
        else
             bbox = [bbox(1),bbox(2),bbox(3),bbox(4)];
        end
         
        img0  = imcrop(img0, bbox);
        img1  = imcrop(img1, bbox);

        [input0, init0] = getLandmark(img0,refShape,[0,0,bbox(3),bbox(4)]);
        [input1, init1] = getLandmark(img1,refShape,[0,0,bbox(3),bbox(4)]);
        
        
        MaxIter=6;
        
        if frame == 1
             points = Fitting(input0,init0,RegMat,MaxIter);
        else
             points = [points(:,1), points(:,2)];
        end

        [imgFeature0,bboxFeat0] = getFeaturesFace(points,input0,tipeFeat);
        [imgFeature1,bboxFeat1] = getFeaturesFace(points,input1,tipeFeat);
        
%         disp([ 'Frame ' num2str(frame) ' BBOX0 ' num2str(bboxFeat0{5}) ' BBOX1 ' num2str(bboxFeat1{5})]);
        
% %          if(frame == 2)
%              figure(1), imshow(img0),  title(sprintf('Frame %d',frame));hold on;
% %              plot(points(:,1),points(:,2),'g*','MarkerSize',6);  
% 
%              rectangle('Position',bboxFeat1{1},'EdgeColor','r') 
%              rectangle('Position',bboxFeat1{2},'EdgeColor','r')
%              rectangle('Position',bboxFeat1{3},'EdgeColor','r')
%              rectangle('Position',bboxFeat1{4},'EdgeColor','r')
%              rectangle('Position',bboxFeat1{5},'EdgeColor','r')
%              rectangle('Position',bboxFeat1{6},'EdgeColor','r')
%           hold off;
%          end         
%           imwrite(imgFeature0,['results\coba\0_' imFol(frame).name]) 
%           imwrite(imgFeature1,['results\coba\1_' imFol(frame).name]) 
        
        head{1} = 'Frame';
        if(strcmp('All',tipeFeat))
            [poc{1}, out_EBL{frame}]    = getPOC(imgFeature0{1,1}, imgFeature1{1,1},frame,'EyeBrowLeft');
            [poc{2}, out_EBR{frame}]    = getPOC(imgFeature0{1,2}, imgFeature1{1,2},frame,'EyeBrowRight');
            [poc{3}, out_EL{frame}]     = getPOC(imgFeature0{1,3}, imgFeature1{1,3},frame,'EyeLeft');
            [poc{4}, out_ER{frame}]     = getPOC(imgFeature0{1,4}, imgFeature1{1,4},frame,'EyeRight');
            [poc{5}, out_Mth{frame}]    = getPOC(imgFeature0{1,5}, imgFeature1{1,5},frame,'Mouth');
            [poc{6}, out_Frhd{frame}]   = getPOC(imgFeature0{1,6}, imgFeature1{1,6},frame,'Forehead');

        else
            [poc{frame},output{frame}]  = getPOC(imgFeature0, imgFeature1,frame, tipeFeat);
        end

        pause(0.001);
        
%        frame = frame + 1;
    end
%     return;
    
    disp('Get Quiver...........');
    % Untuk menampilkan data Quiver dataset di Panel Workshop dataQuiver
    if (strcmp('All',tipeFeat))
        dataQuiver{1,1} = getCoordinate(out_EBL,sMax); % Cell 1 Alis Kiri
        dataQuiver{1,2} = getCoordinate(out_EBR,sMax); % Cell 2 Alis Kanan
        dataQuiver{1,3} = getCoordinate(out_EL,sMax);  % Cell 3 MataKiri
        dataQuiver{1,4} = getCoordinate(out_ER,sMax);  % Cell 4 Mata Kanan
        dataQuiver{1,5} = getCoordinate(out_Mth,sMax); % Cell 5 Mulut
        dataQuiver{1,6} = getCoordinate(out_Frhd,sMax);% Cell 6 Dahi
    else
        dataQuiver = getCoordinate(output,sMax,'true'); % sesuai tipeFeat
    end
else
    disp('Folder Tidak Ditemukan');
end

